﻿# Spike Report

## Particle Effects

![Each of the particle systems in the editor][Particles01]

### Introduction

Particle systems are even more complicated than material systems, and we want to make good looking particles, right?

We need to come to grips with the very basics of particles, as making particle systems is part of the role of a technical artist.

### Goals

1. Tech: What are the particle systems in Unreal Engine (Cascade Particle Editor) capable of?
1. Skill: How do convert a particle brief (“designers says we need X”) into a plan for a particle?
1. Knowledge: What are the different options available to us in UE4 particle systems?

### Personnel

* Primary - Max Finn
* Secondary - N/A

### Technologies, Tools, and Resources used

* [Cascade Particle Systems](https://docs.unrealengine.com/latest/INT/Engine/Rendering/ParticleSystems/index.html)
* [Particle System User Guide](https://docs.unrealengine.com/latest/INT/Engine/Rendering/ParticleSystems/UserGuide/)
* [Intro to Cascade](https://www.youtube.com/watch?v=OXK2Xbd7D9w&list=PLZlv_N0_O1gYDLyB3LVfjYIcbBe8NqR8t)

### Tasks Undertaken

1. Create a particle system called RocketLauncherExhaust, and drag it into the scene.
    1. Open RocketLauncherExhaust, bringing up the Cascade editor.
    1. In the existing emitter, add the GPU sprites module, then go into the Required module details and drop in a smoke texture.
    1. To simulate the rocket flying away, use a Distribution Vector Constant Curve to move the Initial Location, and also animate Colour over Life to allow the smoke to flare brightly, before darkening down to a brown colour.
    1. Particle size is also increased over time to simulate the smoke expanding. Overall, a given particle will live for between 2.0 and 2.5 seconds.
1. Create a particle system called RainSystem.
    1. Of importance in this particle would be setting two cloud emitters to spawn particles within cylinder volumes to approximate the shape of a full cloud, and spawning a rain particle within this being pulled down with Const Acceleration. As an added touch, we can also include a lightning particle which spawns within the cloud area every few seconds (Spawn value of 0.3), and adjust its initial colour value to be roughly 30.0 in R, G, and B to make the lightning texture glow.
1. To create a multicoloured laser beam, set a stream of particles jetting out in t X direction. To manage the high output, we will make this a GPU Sprite emitter.
    1. Setup the colour change through Colour over Life, again overexposing the values for a glowing effect.
    1. From the base of the laser, set a secondary particle emitter that ejects particles over a greater area, and lower the Spawn rate considerably.
1. Create a particle system called FragmentationGrenade.
    1. Set two particle emitters that eject particles over most of the upper Z range (X and Y ranges of 400 to -400).
    1. Use Const Acceleration again to bring these particles back down.
    1. Apply the Mesh TypeData to one of the emitters, and assign a mesh that represents a chunk of grenade debris.
    1. The regular particle emitter can make use of Color over Life as previous particles have done.

### What we found out

Particle systems in Unreal Engine can be used to simulate all sorts of things, from smoke and sparks to lightning bolts and strangely behaving fluids. It can produce all manner of magical effects and even something akin to motion graphics.

Beyond standard sprite based particle effects, which are responsible for a great deal of particle effects made, the Cascade Particle editor also includes AnimTrail, Beam, GPU Sprites, Mesh, and Ribbon emitter types.

The first step in taking a particle brief and converting it into a meaningful particle plan, is to break the particle system (such as a rain cloud) down into individually describable parts. In the case of a rain cloud, this might be a rain component, one or more cloud components (there might be one base cloud particle that forms the base of the cloud, and then be complemented by a second which is lighter and sits on top, moving more calmly). Finally, you might have a particle which simulates lightning flashes. In any case, components are best broken up by unique behaviour or appearance.

As mentioned above, there are a number of different particle types that can be used as the base for a particle emitter. Particle systems allow the combination of multiple particle emitters to layer up the effects until we have our complete effect. Within these particle emitters, there can be a large number of modules which control individual behaviours of a given particle, such as where each particle appears, their colour and size, details about how they move and progress through their lifespans.

Upon initially setting up each of these particle systems, I had concerns about their ability to render well on mobile devices. Having previewed the project in Mobile Viewer mode however, it would appear that all the systems are still able to render at an acceptable level (the GPU Sprites beam suffers odd jagged edges when viewed from angles too far away from level 90 degrees, but this is minor). This said though, it is still almost certain that a mobile device would encounter performance issues sooner than a PC with excessive use of particle systems such as these.

The rain and laser systems had two main possibilities for generating them. In the rain system's case, the cloud sprites could be done as either Translucent or Masked blending modes. The Translucent method allows for 0-255 opacity, letting cloud particles overlap more softly. This leads to higher quad overdraw though, and is likely detrimental to performance. The Masked method is limited to full or zero opacity, and as such more will be immediately occluded so quad overdraw may be lower in comparison. As for the laser system, the logical method to create a beam would be through the Beam TypeData, but it is not immediately apparent as to how we might make a multi-coloured look for the complete laser. As a replacement method, I switched to GPU Sprites and sent a stream of particles closely package to emulate a singular beam. This is surprisingly less computationally heavy than one would expect, but nevertheless would likely be more effectiently done with a single quad as a Beam.

### Open Issues/Risks

1. Particle systems which need visuals in the line of smoke or clouds have proven to have very high overdraw rates as a result of the layering of dozens of semi-translucent materials upon one another. While this is not proving to be an issue in the current small scene with only a few things contained in it, there may be more noticeable issues in a game at greater scale, or even simply on something like a mobile platform.
    * One solution for the cloud issue would be to reduce the number of required particles by supplying a larger cloud sprite to use. Further, if this sprite were animated, it would allow for even more natural integration of even just a few particles at a time, greatly reducing the translucent layering.
1. While one of the aims of the spike was to link the rain cloud to the Pawn so as to simulate having a personal rain cloud, I ran into difficulty setting up a custom Pawn that wouldn't also require setting up a custom PlayerController too.
    * Owing to time constraints, I have opted to leave this part out, as had I managed to get a working Pawn in, it would have almost certainly been as simple as dropping a RainSystem instance into the Pawn blueprint.

### Recommendations

It would be of benefit to investigate more thoroughly the optimisations and best practices available for creating efficient and yet still visually impressive particle systems. This may also lead to further research on the side of material optimisations.

[Particles01]: https://monosnap.com/file/RCr7cGt4VGWXdgmkjYH6boDW9hAqIU.png "Particles in the editor"